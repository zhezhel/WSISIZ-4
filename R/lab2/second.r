butelki <- read.csv("~/WSISIZ/R/lab2/butelki.csv", sep="")
dim(butelki)
head(butelki)
cisnienie=butelki$strength*0.0068947
head(butelki)
cisnienie


#mozna zbudowac istniejaca ramke

butelki$cisn=butelki$strength=0.0068947
head(butelki)

#dzielimy okno graficzne

par(mfrow=c(2,2))

hist(cisnienie)
hist(cisnienie, labels = T)
hist(cisnienie, prob=T ,labels = T)

h <- hist(cisnienie)
h

#zmiana liczby klas
hist(cisnienie, breaks = 10)

hist(cisnienie, prob=T, breaks = "FD" ,labels = T)
hist(cisnienie, prob=T, breaks = "Scott" ,labels = T)
hist(cisnienie, prob=T, breaks = "Sturges" ,labels = T)
layout(1)

boxplot(cisnienie)
boxplot(cisnienie, horizontal = T)
b <- boxplot(cisnienie)
b
b$out

summary(cisnienie)

x = cisnienie

length(x)
range(x)
min(x)
max(x)
mean(x)
var(x)
sd(x)
median(x)
mean(x, trim = 0.1)
sum(x)
quantile(x, 0.2)
IQR(x)


library(fBasics)
skewness(x)
kurtosis(x)
#wsp.zmienności
(v=sd(x)/mean(x))

rzad = c(0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95)

sum(h$mids * h$counts) / sum(h$counts)

auto <- read.csv2("~/WSISIZ/R/lab2/samochody.csv")
View(samochody)

head(auto)
auto$zp <- 378.5/(1.609*auto$mpg)
auto$zp
summary(auto)
summary(auto$zp)
mean(auto$zp, na.rm=T)
hist(auto$zp)

# Zadanie 3 ####

auto$zp.kat[auto$zp <= 7] = 'm'
auto$zp.kat[auto$zp > 7 & auto$zp <= 10] = 's'
auto$zp.kat[auto$zp > 10] = 'd'
auto$zp.kat

(tt=table(auto$zp.kat))
prop.table(tt)*100
round(prop.table(tt)*100,1)

barplot(tt)

# Zadanie 4 ####
head(auto)

mean(auto$zp[auto$producent==1], na.rm = T)
mean(auto$zp[auto$producent==2], na.rm = T)
mean(auto$zp[auto$producent==3], na.rm = T)
tapply(auto$zp, auto$producent, mean, na.rm = T)

bb = boxplot(auto$zp~auto$producent)
bb
bb$out[bb$group==2]

# Zadanie 5 ####

boxplot(auto$zp~auto$cylindry)

# Zadanie 7 ####

mean(auto$zp)

# Zadanie 8 ####
# 8A ####
x = auto$moc[auto$rok >= 79 & auto$rok <= 81]
x
boxplot(x)
mean(x, na.rm = T)
# 8C ####
quantile(x, 0.95, na.rm=T)

x = auto$mpg[auto$waga  >= 2500 & auto$waga <= 3000 ]


x = auto$waga[auto$mpg  > 26  ]
auto$mpg
x
quantile(x, 0.95, na.rm = T)


ceny = c(23.30, 24.30, 24.50, 26.10, 25.30, 23.10, 25.30, 25.50, 24.30, 22.60, 24.80, 24.60, 25.20, 24.30, 24.50, 25.40, 24.60, 25.20, 24.10, 26.80 )
par(mfrow=c(3,1))

plot(ceny)
plot(ceny, type = 'l')
plot(ceny, type = 'b')
plot(ceny, type = 'b', pch = 16, lty=2)
plot(ceny, type = 'b', pch = 20, lty=3)
plot(ceny, type = 'b', pch = 16, lty=2)

czynsz = c(334, 352, 384, 436, 405, 498, 425, 392, 374, 398, 403, 389, 424, 344, 367, 429, 400, 457, 392, 424, 409, 428, 443, 454, 339, 378, 345, 389, 387, 422)

obwod = function(x, y){
  ob = 2*(x+y)
  list(obwod_prostokąta = ob)
}

pole = function(x, y){
  pole = x*y
  ob = 2*(x+y)
  list(obwod_prostokąta = ob, polepr = pole)
}

pole(22,22)$polepr

pdf("lol.pdf")
plot(ceny)
plot(ceny, type = 'l')
plot(ceny, type = 'b')
plot(ceny, type = 'b', pch = 16, lty=2)
plot(ceny, type = 'b', pch = 20, lty=3)
plot(ceny, type = 'b', pch = 16, lty=2)
dev.off()
