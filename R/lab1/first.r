# Zadanie 2
qnorm(0.95)
qnorm(0.95, 0, 1)
qnorm(.95)

qnorm(0.975)
qnorm(0.99, 3, 2) # b* 0.99 N(3, 2)

#qt(p,n) p = rząd, n = liczba stopni swobody
qt(0.95, 10)
qt(0.99, 20)
qchisq(0.9, 4) # qchisq (p, n)
qchisq(0.95, 10)

qf(0.95, 2 ,10)
qf(0.99, 3, 18)

# Zadanie 3

pnorm(179, 173, 6)
pnorm(180, 173, 6) - pnorm(167,173,6)
1 - pnorm(181, 173, 6)
qnorm(0.6, 173, 6)

# Z adanie 5

dbinom(5,5,0.95)

1- pbinom(2,5,0.95)

# Zadanie 6

pbinom(0,5,0.95)
1 - ppois(2,3)

#  Zadanie 7
# a
1 - pexp(1000, 0.0001)
1 - pexp(10000, 0.0001)
1 - pexp(30000, 0.0001)
# b
qexp(0.1, 0.0001)
# c
pexp(0.5, 4)
# d

dpois(0,4)
1-pexp(1,4)

# Zadanie 1
curve(dnorm(x))
curve(dnorm(x), xlim = c(-5,5))
curve(dnorm(x), xlim = c(-5,5), ylim = c(0,0.8))
curve(dnorm(x, 1, 1),add=T, col=2)

curve(1- pnorm(x))
# Zadanie 4

curve(dt(x,1), xlim = c(-5,5), ylim = c(0, 0.5), col = 1)
curve(dt(x,5),add=T, col = 2)
curve(dt(x,30),add=T, col = 3)
curve(dnorm(x),add=T, col = 6)

# Zadanie 9
panie = c(17364, 56128, 11239, 8170)
etykiety = c("panny", "mezatki", "wdowy", "rpzwódki")
par(mfrow=c(1,1))
pie(panie)
pie(panie, labels = etykiety, radius = 1,cex=0.6)
pie(panie, radius = 1,cex=0.6, labels =paste(etykiety, panie, sep=" ", main="Stan") )

pie(panie,radius=1,cex=0.8,
    labels=paste(etykiety,panie,paste(proc,'%',''),
                 sep = "\n"), main="Stan cywilny")



# Zadanie 10

stacje <- read.csv("~/WSISIZ/R/lab1/stacje.csv", sep="")
(licz=table(stacje))
(licz=100*prop.table(licz))
pie(licz)

pie(licz, labels=licz)
(names(licz))


####
head(iris)
summary(iris)
names(iris)
colnames(iris)
mean(iris$Sepal.Length)
iris[10,3]
iris[10,]
iris[c(1,5:10,20),c(2,5)]
mean(iris$Petal.Width[iris$Species=="setosa"])
mean(iris$Petal.Length>4 )
var(iris$Petal.Width[iris$Species == 'versicolor'] & iris$Petal.Length > 4)
quantile(iris$Petal.Length, 0.9)

library(MASS)
