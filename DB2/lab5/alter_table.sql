ALTER TABLE ZAMSZCZ
DROP CONSTRAINT rezamszczIDZam


ALTER TABLE ZAMSZCZ
MODIFY IDZam
REFERENCES zamowienia(IDZam)
ON DELETE CASCADE

UPDATE towary
SET
	cena=cena*1.2

UPDATE klienci
SET 	address = 'Lublin',
	platnikVAT = 'N'
