SELECT last_name 
FROM employees 
WHERE last_name LIKE '%1_%' ESCAPE '1';

SELECT last_name zz, job_id, salary, department_id
FROM employees ORDER BY 2 DESC, 3 DESC NULLS FIRST;

SELECT last_name zz, job_id, salary, department_id
FROM employees department_id NULLS FIRST;

SELECT CONCAT('Hello', 'World') AS cancat  FROM dual;	-- HelloWorld
SELECT SUBSTR('HelloWorld', 1, 5) AS substr FROM dual;	-- Hello

SELECT LENGTH('HelloWorld') AS length FROM dual;	-- 10
SELECT INSTR('HelloWorld', 'W')	AS instr FROM dual;	-- 6
SELECT LPAD(salary, 10, '*') AS lpad FROM dual;		-- *****24000
SELECT RPAD(salary, 10, '*') AS rpad FROM dual;		-- 24000*****
SELECT REPLACE('JACK and JUE', 'J', 'B') AS replace FROM dual;	-- BLACK and BLUE
SELECT TRIM('H', FROM 'HelloWorld') AS trim FROM dual;	-- elloWorld

SELECT INSTR('abcdefghfhsdkfwelkfjwljsdkwe', 'sdk', -10, 1) FROM dual;	-- 11
SELECT INSTR('abcdefghfhsdkfwelkfjwljsdkwe', 'sdk', 1, 2) FROM dual;	-- 24

SELECT ROUND(45.926, 2)	AS round FROM dual;	-- 45.93
SELECT TRUNC(45.926, 2)	AS trunc FROM dual;	-- 45.92
SELECT MOD(1600, 300) AS mod FROM dual;		-- 100

SELECT last_name, (SYSDATE - hire_date)/7 AS WEEKS
FROM employees
WHERE department_id = 90;

SELECT SYSDATE, TO_CHAR(SYSDATE, 'dd ", of" Month yyyy') FROM dual;
	-- 20.04.11 | 20 , of April 2011

SELECT last_name, TO_CHAR(salary, '999G999D00') FROM employees;
	-- King | 24 000,00

SELECT last_name, TO_CHAR(salary, '999G999D00L', 'nls_numeric_characters=".,"')
FROM employees;
	-- King | 24,000.00p.

SELECT COUNT(DISTINCT department_id)
FROM employees;
	-- 7 # Return number of unique rows

SELECT AVG(NVL(commission_pct, 0), SUM(commission_pct)/COUNT(commission_pct)
FROM employees;
	-- 0,0425 | 0,2125 # COUNT not add NULL 

SELECT department_id, job_id, sum(salary) 
FROM employees
GROUP BY job_id, department_id 
ORDER BY 1;

SELECT department_id, SUM(salary)
FROM employees
GROUP BY department_id
HAVING SUM(SALARY) > 1000;

SELECT department_id, department_name, city
FROM departments
NATURAL JOIN locations;

SELECT last_name, salary, department_name
FROM departments
JOIN employees
USING (department_id);

SELECT e.last_name, e.salary, d.department_name, department_id
FROM departments d
JOIN employees e
USING (department_id);

ALTER TABLE departments RENAME COLUMN department_id TO dept_id;

SELECT e.last_name, e.salary, d.department_name, department_id, city
FROM departments d
JOIN employees e
ON (e.department_id = d.dept_id)
JOIN locations
USING (location_id);

SELECT worker.last_name emp, manager.last_name mgr
FROM employees worker 
JOIN employees manager
ON (worker.manager_id = manager.employee_id);

SELECT e.last_name, e.salary, j.grade_level
FROM employees e
JOIN job_grades j
ON e.salary
BETWEEN j.lowest_sal AND j.highest_sal;

