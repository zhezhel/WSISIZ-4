CREATE TABLE towary
(
	IDTowaru NUMBER(5),
	nazwa VARCHAR2(50),
	cena NUMBER(7,2)
);

CREATE TABLE klienci
(
	IDKlienta NUMBER(5),
	firma VARCHAR2(50),
	adres VARCHAR2(40),
	platnikVAT CHAR(1)
);
