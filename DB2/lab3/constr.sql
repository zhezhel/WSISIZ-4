SET TERMOUT off
COLUMN table_name FORMAT A15
COLUMN constraint_name FORMAT A15
COLUMN constraint_type FORMAT A10
COLUMN r_constraint_name FORMAT A15
COLUMN search_condition FORMAT A20
REM COLUMN constraint_type HEADING "CONSTRAINT|TYPE"
COLUMN constraint_type HEADING "CONSTR|TYPE"
COLUMN r_constraint_name HEADING RC_NAME
COLUMN search_condition HEADING CONDITION
COLUMN constraint_name HEADING CONSTRAINT

COLUMN column_name FORMAT A15
COLUMN column_name HEADING COLUMN
COLUMN table_name HEADING RTABLE|NAME
SET TERMOUT on

SET VERIFY off
SELECT        
       c1.constraint_name,
       --- table_name,
       c2.table_name,
       c3.column_name,
       c1.constraint_type,
       c1.search_condition
    FROM user_constraints c1, user_constraints c2, user_cons_columns c3
WHERE c1.r_constraint_name=c2.constraint_name(+) AND c1.table_name=UPPER('&1')
AND c1.constraint_name=c3.constraint_name(+);
SET VERIFY on

