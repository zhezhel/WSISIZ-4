CREATE TABLE towary(
	IDTowary NUMBER(5) CONSTRAINT pk_towary PRIMARY KEY,
	nazwa VARCHAR2(50) CONSTRAINT nn_towary_nazwa NOT NULL,
	cena NUMBER(7,2) CONSTRAINT ch_towary_cena CHECK(cena > 0)
)
