DROP TABLE klienci;
CREATE TABLE klienci(
	IDKlienta NUMBER(5) CONSTRAINT pk_klienci PRIMARY KEY,
	firma VARCHAR2(50) CONSTRAINT nn_klienci_firma NOT NULL,
	adres VARCHAR2(40) CONSTRAINT nn_klienci_adres NOT NULL,
	platnikVAT CHAR(1) DEFAULT('T') NOT NULL CONSTRAINT ch_platnikVAT CHECK(platnikVAT IN ('T', 'N')),
)
/
