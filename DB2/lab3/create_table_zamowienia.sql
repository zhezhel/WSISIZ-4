CREATE TABLE zamowienia(
	IDZam NUMBER(5) PRIMARY KEY,
	nazwa VARCHAR2(50) CONSTRAINT nn_towary_nazwa NOT NULL,
	cena NUMBER(7,2) CONSTRAINT ch_towary_cena CHECK(cena > 0)
)
/
