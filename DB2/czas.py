#!/usr/bin/env python3
import datetime
from prettytable import PrettyTable
from math import log2


class Unsorted(object):
    '''Class for calculating speed of database access to files'''
    def __init__(self, **kwargs):
        self.N = kwargs['N']
        self.R = kwargs['R']
        self.B = kwargs['B']
        self.D = kwargs['D']
        self.C = kwargs['C']
        self.H = kwargs['H']
    
    def blocking_factor(self):
        '''Blocking factor'''
        return self.B / self.R

    def free_space(self):
        '''Free space in file'''
        return self.B - (self.blocking_factor() * self.R)

    def insert(self):
        return 2 * self.D + self.C

    def search_avg(self):
        return 0.5 * self.N * ( self.D + self.R * self.C )

    def search_max(self):
        return self.N * ( self.D + self.R * self.C )

    def lookover(self):
        return self.N * ( self.D + self.R * self.C )

    def search_ranged(self):
        return self.N * ( self.D + self.R * self.C )

    def del_max(self):
        return self.N * ( self.D + self.R * self.C ) + ( self.C + self.D )

    def del_avg(self):
        return 0.5 * self.N * ( self.D + self.R * self.C ) + ( self.C + self.D )

class Sorted(object):
    '''Class for calculating speed of database access to files'''
    def __init__(self, **kwargs):
        self.N = kwargs['N']
        self.R = kwargs['R']
        self.B = kwargs['B']
        self.D = kwargs['D']
        self.C = kwargs['C']
        self.H = kwargs['H']
    
    def blocking_factor(self):
        '''Blocking factor'''
        return self.B / self.R

    def free_space(self):
        '''Free space in file'''
        return self.B - (self.blocking_factor() * self.R)
    
    def insert(self):
        return 2 * ( 0.5 * self.N * (self.D + self.R * self.C))

    def lookover(self):
        return self.N * ( self.D + self.R * self.C )

    def search(self):
        return self.D * log2(self.B) + self.C * log2(self.R)

    def search_ranged(self):
        return self.D * log2(self.B) + self.C * log2(self.R) + self.N * self.D
        
    def deleting(self):
        return 2 * ( 0.5 * self.N * (self.D + self.R * self.C))

class PrintData():
    def __init__(self, **kwargs):
        self.Sorted = Sorted(N=kwargs['N'], R=kwargs['R'], B=kwargs['B'], D=kwargs['D'], C=kwargs['C'], H=kwargs['H'])
        self.Unsorted = Unsorted(N=kwargs['N'], R=kwargs['R'], B=kwargs['B'], D=kwargs['D'], C=kwargs['C'], H=kwargs['H'])

    def to_hr_time(self, milli):
        return milli/1000 #str(datetime.timedelta(milliseconds=milli))

    def __str__(self):
        table = PrettyTable()
        table.field_names = ["N", "Operacja", "Pliki uporzadkowane", "Pliki nieuporzadkowane"]
        table.add_row([1, "Przeglądanie", self.to_hr_time(self.Sorted.lookover()), self.to_hr_time(self.Unsorted.lookover())])
        table.add_row([2, "Wyszukiwanie", self.to_hr_time(self.Sorted.search()), self.to_hr_time(self.Unsorted.search_avg())])
        table.add_row([3, "Wyszukiwanie z P", self.to_hr_time(self.Sorted.search_ranged()), self.to_hr_time(self.Unsorted.search_ranged())])
        table.add_row([4, "Wstawianie", self.to_hr_time(self.Sorted.insert()), self.to_hr_time(self.Unsorted.insert())])
        table.add_row([5, "Usuwanie", self.to_hr_time(self.Sorted.deleting()), self.to_hr_time(self.Unsorted.del_avg())])
        return table.get_string()

a = PrintData(N=30000, R=0000, B=1024, D=15, C=0.005, H=0.005)
print(a)
