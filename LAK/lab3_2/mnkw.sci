function [a,b] = mnkw(x,y)
    n = length(x);
    X2 = sum(x.*x);
    X = sum(x);
    XY = sum(x.*y);
    Y = sum(y);
    delta = (n*X2-X*X);
    a = (n*XY-X*Y)/delta; 
    b = (X2*Y-XY*X)/delta; 
endfunction
