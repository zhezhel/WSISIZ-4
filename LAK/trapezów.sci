funcprot(0)

function val=functionValue(string, x)
    val = evstr(string)
endfunction

function result=calkujT(string, xp, xk, n)
    dx = (xk - xp) / n
    result = 0

    for i=0:n
        result = result + functionValue(string, xp+i * dx)
    end
    
    result = result + (functionValue(string, xp) + functionValue(string, xk)) / 2
    result = result * dx
endfunction


function result=calkujP(string, xp, xk, n)
    dx = (xk - xp) / n
    result = 0

    for i=1:n
        result = result + functionValue(string, xp+i * dx)
    end
    
    result = result * dx
endfunction


function result=randomPoint(a, b)
    result = a + rand() * (b-a)
endfunction


function result=calkujCM(string, xp, xk, n)
    dx = (xk - xp) / n
    result = 0

    for i=0:n
        result = result + functionValue(string, randomPoint(xp, xk))
    end
    
    result = (result / n ) * (xk - xp)
endfunction

