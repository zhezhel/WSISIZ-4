#!/usr/bin/python
# -*- coding: utf-8 -*-

# Metoda trapezów

from math import * # dzięki temu user będzie mógł podać dowolny wzór używając
# funkcji matematycznech
def f(x):
    global wzor #pobranie zmiennej globalnej
    return eval(wzor) #parsuje wzór podany przez użytkownika
 
print("Podaj wzór: ")
wzor=raw_input() #użytkownik może wprowadzić własny wzór funkcji
#np: "x*x + 2*x - 5"
print("Podaj początek argumentów")
A=float(raw_input())
print("Podaj koniec argumentów")
B=float(raw_input())
print("Podaj ilość kroków")
k=float(raw_input())
P=A #liczony punkt startowy
szer=(B-A)/k #"wysokość" trapezu
wynik=0.0
while P<B:
    wynik+= szer * ((f(P) + f(P+szer))/2.0) #obliczenie pola trapezu
    #       podstawy mają długości f(P) i f(P+szer)
    P+=szer
print("f("+str(P)+"): "+str(f(P)))
print("Wynik: "+str(wynik))
