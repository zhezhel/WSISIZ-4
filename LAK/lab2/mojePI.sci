function pi=mojePI1(n)
suma=0
for k=1:n
    suma=suma+1/k^2
end
pi = sqrt(6*suma)
disp(pi)
endfunction

function pi=mojePI2(n)
suma=0
for k=1:n
    suma=suma + ((-1)^(k+1))/k^2
end
pi = sqrt(12*suma)
disp(pi)
endfunction
