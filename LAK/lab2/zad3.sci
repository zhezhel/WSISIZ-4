function [y]  = direrichleta(x)
	m = length(x)
	y = zeros(x)
	for i = 1:m do 
		for k = 1:10^4 do
			y(i) = y(i) +1/k^x(i)
		end
	end
endfunction
x = linspace(1.05, 5, 50)
y = direrichleta(x)
clf
plot(x,y)
xtitle('Andrzej Żeżel','x','D(x)')
