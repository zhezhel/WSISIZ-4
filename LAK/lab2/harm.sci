function y=harm(n)
    H(1)=1
    for k=2:n do
        H(k)=H(k-1)+1/k
    end
    y=H(n)
endfunction

for k=1:100
    x(k)=k
    y(k)=harm(k)
end

plot(x,y,'.b')
plot(x,log(x),'.y')
xtitle('Andrzej Żeżel')
