function [a,b] = mnkw(x,y)
    	n = length(x);
        X2 = sum(x.*x);
	X = sum(x);
	XY = sum(x.*y);
	Y = sum(y);
	delta = (n*X2-X*X);
	a = (n*XY-X*Y)/delta;
	b = (X2*Y-XY*X)/delta;
endfunction

function pi=mojePI1(n)
	suma=0
	for k=1:n
    		suma=suma+1/k^2
	end
	pi = sqrt(6*suma)
	disp(pi)
endfunction

function pi=mojePI2(n)
	suma=0
	for k=1:n
    		suma=suma + ((-1)^(k+1))/k^2
	end
	pi = sqrt(12*suma)
	disp(pi)
endfunction


for k=1:1000
    x(k)=k
    pi1(k)=mojePI1(k)
    pi2(k)=mojePI2(k)
end

err1=abs(pi1-%pi)
err2=abs(pi2-%pi)

X=log(x)
Y1=log(err1)
Y2=log(err2)

clf
plot(X,Y1,'.r')
plot(X,Y2,'.b')

[a1, b1] = mnkw(X, Y1)
[a2, b2] = mnkw(X, Y2)

plot(a1*x+b1)
plot(a2*x+b1)
