function funValue(fun, x)
{
return eval(fun);
}
 
//fun - string - function to integrate (for exaple "x*x + 3")
//xp - float - start of the range to integrate
//xk - float - end of the range to integrate
//n - integer - precision
function calculate(fun, xp, xk, n){
  //width of the one interval
  var dx = (xk - xp) / n;
 
  //initialize result
  var result = 0;
 
  for (var i=1; i<n; i++){
    result += funValue(fun, xp+i * dx);}
 
  result += (funValue(fun, xp) + funValue(fun, xk)) / 2;
  result *= dx;
 
  return result;
}

console.log(calculate("x*x*x+x*x*x*x*x*x",0,1,1000));
