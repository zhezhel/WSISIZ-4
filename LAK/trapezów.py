#!/usr/bin/env python3

def functionValue(string, x):
    return eval(string)


def calculate(string, xp, xk, n):
    dx = (xk - xp) / n
    result = 0
    for i in range(n):
        result += functionValue(string, xp+i * dx)
    result += (functionValue(string, xp) + functionValue(string, xk)) / 2
    result *= dx
    return result

print(calculate("x*x*x+x*x*x*x*x*x", 0, 1, 10))
